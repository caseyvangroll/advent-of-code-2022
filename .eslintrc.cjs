module.exports = {
  extends: ["airbnb-base", "airbnb-typescript/base"],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    tsconfigRootDir: __dirname,
    project: ["./tsconfig.json"],
  },
  plugins: ["@typescript-eslint"],
  root: true,
  ignorePatterns: ["*.cjs"],
  rules: {
    "no-await-in-loop": 0,
    "no-console": 0,
    "linebreak-style": 0,
    "max-classes-per-file": 0,
    "max-len": ["error", { code: 120 }],
    'prefer-destructuring': 0,
  },
};
