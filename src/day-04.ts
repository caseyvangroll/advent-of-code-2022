// https://adventofcode.com/2022/day/4

export function doesOneFullyContainTheOther(
  minA: number,
  maxA: number,
  minB: number,
  maxB: number,
) {
  return (minA <= minB && maxB <= maxA) || (minB <= minA && maxA <= maxB);
}

export function doesOverlap(
  minA: number,
  maxA: number,
  minB: number,
  maxB: number,
) {
  return (maxA >= minB && minA <= minB)
    || (minA <= maxB && maxA >= maxB)
    || doesOneFullyContainTheOther(minA, maxA, minB, maxB);
}

export function getGroups(line: string) {
  return (line.match(/\d+/g) || [0, 0, 0, 0]).map(Number);
}

export default function day4(input: string) {
  const lines = input.replace(/\r/g, '').split('\n');
  let pairs = 0;
  let overlapPairs = 0;
  lines.forEach((line) => {
    const [minA, maxA, minB, maxB] = getGroups(line);
    if (doesOneFullyContainTheOther(minA, maxA, minB, maxB)) {
      pairs += 1;
    }
    if (doesOverlap(minA, maxA, minB, maxB)) {
      overlapPairs += 1;
    }
  });

  console.log('In how many assignment pairs does one range fully contain the other?', pairs);
  console.log('In how many assignment pairs do the ranges overlap?', overlapPairs);
}
