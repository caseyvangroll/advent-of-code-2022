// https://adventofcode.com/2022/day/8

export function getVisibleTreeCt(heightMap: number[][]): number {
  const visible: any = {};
  const width = heightMap[0].length;
  const height = heightMap.length;
  const setVisible = (x: number, y: number) => {
    visible[`${x},${y}`] = true;
  };

  // top to bottom
  let tallest: number;
  let x: number;
  let y: number;
  for (x = 0; x < width; x += 1) {
    tallest = -1;
    y = 0;
    while (y < height && tallest !== 9) {
      if (heightMap[y][x] > tallest) {
        tallest = heightMap[y][x];
        setVisible(x, y);
      }
      y += 1;
    }
  }
  // bottom to top
  for (x = 0; x < width; x += 1) {
    tallest = -1;
    y = height - 1;
    while (y >= 0 && tallest !== 9) {
      if (heightMap[y][x] > tallest) {
        tallest = heightMap[y][x];
        setVisible(x, y);
      }
      y -= 1;
    }
  }

  // left to right
  for (y = 0; y < height; y += 1) {
    tallest = -1;
    x = 0;
    while (x < width && tallest !== 9) {
      if (heightMap[y][x] > tallest) {
        tallest = heightMap[y][x];
        setVisible(x, y);
      }
      x += 1;
    }
  }

  // right to left
  for (y = 0; y < height; y += 1) {
    tallest = -1;
    x = width - 1;
    while (x >= 0 && tallest !== 9) {
      if (heightMap[y][x] > tallest) {
        tallest = heightMap[y][x];
        setVisible(x, y);
      }
      x -= 1;
    }
  }

  let visibleTreeCt = 0;
  let forestAsciiDisplay = '';
  for (y = 0; y < height; y += 1) {
    for (x = 0; x < width; x += 1) {
      if (visible[`${x},${y}`]) {
        forestAsciiDisplay += 'O';
        visibleTreeCt += 1;
      } else {
        forestAsciiDisplay += 'X';
      }
    }
    forestAsciiDisplay += '\n';
  }
  // console.log(forestAsciiDisplay);
  return visibleTreeCt;
}

export function getScore(heightMap: number[][], row: number, column: number): number {
  const width: number = heightMap[0].length;
  const height: number = heightMap.length;

  const viewerHeight = heightMap[row][column];
  let visibleToNorth: number = 0;
  if (row > 0) {
    visibleToNorth = 1;
    for (let y = row - 1; y > 0 && heightMap[y][column] < viewerHeight; y -= 1) {
      visibleToNorth += 1;
    }
  }
  let visibleToEast: number = 0;
  if (column < width - 1) {
    visibleToEast = 1;
    for (let x = column + 1; x < width - 1 && heightMap[row][x] < viewerHeight; x += 1) {
      visibleToEast += 1;
    }
  }
  let visibleToSouth: number = 0;
  if (row < height - 1) {
    visibleToSouth = 1;
    for (let y = row + 1; y < height - 1 && heightMap[y][column] < viewerHeight; y += 1) {
      visibleToSouth += 1;
    }
  }
  let visibleToWest: number = 0;
  if (column > 0) {
    visibleToWest = 1;
    for (let x = column - 1; x > 0 && heightMap[row][x] < viewerHeight; x -= 1) {
      visibleToWest += 1;
    }
  }
  const score = visibleToNorth * visibleToEast * visibleToSouth * visibleToWest;
  return score;
}

export function getHighestScorePossible(heightMap: number[][]): number {
  const width: number = heightMap[0].length;
  const height: number = heightMap.length;
  let highestScore: number = -1;
  for (let row = 0; row < height; row += 1) {
    for (let column = 0; column < width; column += 1) {
      const score = getScore(heightMap, row, column);
      if (score > highestScore) {
        highestScore = score;
      }
    }
  }
  return highestScore;
}

export default function day8(input: string) {
  const heightMap: number[][] = input
    .split('\n')
    .map((line) => line.split('').filter((s) => s.trim() !== '').map((s) => Number(s)));

  const visibleTreeCt: number = getVisibleTreeCt(heightMap);
  console.log('Consider your map; how many trees are visible from outside the grid?', visibleTreeCt);

  const highestScorePossible: number = getHighestScorePossible(heightMap);
  console.log(
    'Consider each tree on your map. What is the highest scenic score possible for any tree?',
    highestScorePossible,
  );
}
