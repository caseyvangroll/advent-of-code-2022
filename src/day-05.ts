// https://adventofcode.com/2022/day/5

export function getStack(stackLines: string[], stackIndex: number) {
  const stack = [];
  const stackX = stackIndex * 4 + 1;
  for (let i = stackLines.length - 1; i >= 0; i -= 1) {
    const stackChar = stackLines[i].charAt(stackX).trim();
    if (stackChar) {
      stack.push(stackChar);
    }
  }
  return stack;
}

enum Mode {
  OneAtATime = 'OneAtATime',
  Batch = 'Batch',
}

export function runProcedure(stacks: string[][], procedure: string[], mode: Mode): string[][] {
  const rearrangedStacks = stacks.map((stack) => [...stack]);
  procedure.forEach((line) => {
    const [countToMove, sourceStackIndex, destStackIndex] = (line.match(/\d+/g) || [0, 0, 0]).map(Number);
    const sourceStack = rearrangedStacks[sourceStackIndex - 1]; // 1-based index
    const destStack = rearrangedStacks[destStackIndex - 1]; // 1-based index

    if (mode === Mode.OneAtATime) {
      for (let i = 0; i < countToMove; i += 1) {
        const item = sourceStack.pop();
        if (item) {
          destStack.push(item);
        }
      }
    } else {
      const tmpStack = [];
      for (let i = 0; i < countToMove; i += 1) {
        const item = sourceStack.pop();
        if (item) {
          tmpStack.push(item);
        }
      }
      while (tmpStack.length) {
        destStack.push(tmpStack.pop() || '');
      }
    }
  });

  return rearrangedStacks;
}

export default function day5(input: string) {
  // Parse Input
  const stackLabelRegex = /^[\d\s]+$/m;
  const match = stackLabelRegex.exec(input);
  if (!match) {
    throw new Error('Unable to find stack labels');
  }
  const startOfStackLabelLine = match.index;
  const stackLines = input
    .substring(0, startOfStackLabelLine)
    .split('\n')
    .filter(Boolean);
  const stackLabelLine = match[0].trim();
  const procedureLines = input
    .substring(startOfStackLabelLine)
    .replace(match[0], '')
    .split('\n')
    .map((s) => s.trim())
    .filter(Boolean);

  const stackCt = Number(stackLabelLine.split(/\s+/g).pop());
  const stacks = new Array(stackCt).fill(undefined).map((_, i) => getStack(stackLines, i));

  const rearrangedStacks = runProcedure(stacks, procedureLines, Mode.OneAtATime);
  const topCrates = rearrangedStacks.map((stack) => stack.pop()).join('');
  console.log('After the rearrangement procedure completes, what crate ends up on top of each stack?', topCrates);

  const rearrangedStacksPart2 = runProcedure(stacks, procedureLines, Mode.Batch);
  const topCratesPart2 = rearrangedStacksPart2.map((stack) => stack.pop()).join('');
  console.log(
    'After the rearrangement procedure completes, what crate ends up on top of each stack? (part 2)',
    topCratesPart2,
  );
}
