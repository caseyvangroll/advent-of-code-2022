export default class File {
  basename: string;

  size: number;

  static regex = /^(\d+)\s+(.+)/;

  constructor(line: string) {
    if (!File.isFile(line)) {
      throw new Error(`Not a valid file: ${line}`);
    }
    const [, size, basename] = File.regex.exec(line) || ['0', 'unknown'];
    this.basename = basename;
    this.size = Number(size);
  }

  toString() {
    return this.basename;
  }

  static isFile(line: string): boolean {
    return File.regex.test(line);
  }
}
