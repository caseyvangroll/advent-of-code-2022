import fs from 'fs';

// Parse inputs
const dayIndex = process.argv[2];
const allSolutionFilenames = new Array(25).fill(undefined).map((_, i) => `day-${String(i + 1).padStart(2, '0')}`);
let selectedSolutionFilenames = allSolutionFilenames;
if (dayIndex && !Number.isNaN(Number(dayIndex))) {
  selectedSolutionFilenames = [`day-${String(dayIndex).padStart(2, '0')}`];
}

async function main() {
  for (let i = 0; i < selectedSolutionFilenames.length; i += 1) {
    const solutionFilename = selectedSolutionFilenames[i];
    const solutionModule = await import(`${process.cwd()}/out/${solutionFilename}`);

    let input;
    // const inputFilename = `./inputs/${solutionFilename}.sample.txt`;
    const inputFilename = `./inputs/${solutionFilename}.txt`;
    if (fs.existsSync(inputFilename)) {
      input = fs.readFileSync(inputFilename, 'utf-8');
    }

    console.log(`Running ${solutionFilename}...`);
    solutionModule.default(input);
  }
}

main();
