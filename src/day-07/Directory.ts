import File from './File';

export default class Directory {
  basename: string;

  parentDirectory: Directory | null;

  children: Array<Directory | File> = [];

  static regex = /^dir\s+(.+)/;

  constructor(parentDirectory: Directory | null, line: string) {
    this.parentDirectory = parentDirectory;
    const isRoot = parentDirectory === null;
    if (isRoot) {
      this.basename = '';
    } else {
      this.basename = Directory.regex.exec(line)?.[1] || '';
    }
    this.children = [];
  }

  addDirectory(line: string) {
    this.children.push(new Directory(this, line));
  }

  addFile(line: string) {
    this.children.push(new File(line));
  }

  addChild(child: Directory | File) {
    this.children.push(child);
  }

  get size(): number {
    return this.children.reduce((sum, child) => sum + child.size, 0);
  }

  toString(): string {
    return `${this.basename} { ${this.children.map((child) => child.toString()).join(' ')} }`;
  }

  static isDirectory(line: string): boolean {
    return Directory.regex.test(line);
  }
}
