export default class CdCommand {
  path: string;

  static regex = /^\$\s+cd\s+(.+)/;

  constructor(line: string) {
    if (!CdCommand.isCdCommand(line)) {
      throw new Error(`Not a valid cd command: ${line}`);
    }

    const [, path] = CdCommand.regex.exec(line) || [];
    this.path = path;
  }

  static isCdCommand(line: string): boolean {
    return CdCommand.regex.test(line);
  }
}
