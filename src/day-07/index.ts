// https://adventofcode.com/2022/day/7
import Directory from './Directory';
import CdCommand from './CdCommand';
import File from './File';

export function getDirsPassingTest(
  fileSystemRoot: Directory,
  testFn: (directory: Directory) => boolean,
) : Directory[] {
  const result = [];
  const directoriesToCheck = [fileSystemRoot];
  while (directoriesToCheck.length) {
    const directoryToCheck = directoriesToCheck.pop();
    if (directoryToCheck && testFn(directoryToCheck)) {
      result.push(directoryToCheck);
    }
    if (directoryToCheck && directoryToCheck.children.length) {
      directoryToCheck.children.forEach((child) => {
        if (child instanceof Directory) {
          directoriesToCheck.push(child);
        }
      });
    }
  }
  return result;
}

export default function day7(input: string) {
  const lines = input.split(/\s*\n\s*/g).slice(1); // skip 'cd /'
  const fileSystemRoot: Directory = new Directory(null, '');
  let cwd: Directory = fileSystemRoot;

  lines.forEach((line) => {
    if (CdCommand.isCdCommand(line)) {
      const cdCommand = new CdCommand(line);
      if (cdCommand.path === '..') {
        cwd = cwd.parentDirectory ?? cwd;
      } else {
        const childDirectory = cwd.children.find((child) => child.basename === cdCommand.path);
        cwd = childDirectory as Directory;
      }
    } else if (Directory.isDirectory(line)) {
      cwd.addDirectory(line);
    } else if (File.isFile(line)) {
      cwd.addFile(line);
    }
  });

  const largestDirs = getDirsPassingTest(fileSystemRoot, (dir) => dir.size <= 100000);
  const sumOfLargestDirSizes = largestDirs.reduce((acc, dir) => acc + dir.size, 0);
  console.log(
    'Find all of the directories with a total size of at most 100000.'
        + 'What is the sum of the total sizes of those directories?',
    sumOfLargestDirSizes,
  );
  const spaceAvailable = 70000000 - fileSystemRoot.size;
  const spaceNeeded = 30000000 - spaceAvailable;
  const possibleDirectoriesToDelete = getDirsPassingTest(fileSystemRoot, (dir) => dir.size >= spaceNeeded);
  possibleDirectoriesToDelete.sort((a, b) => a.size - b.size);
  console.log(
    'Find the smallest directory that, if deleted, would free up enough space on the filesystem to run the update.'
    + 'What is the total size of that directory?',
    possibleDirectoriesToDelete[0].size,
  );
}
