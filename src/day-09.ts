// https://adventofcode.com/2022/day/9

enum Direction {
  Up = 'Up',
  Right = 'Right',
  Down = 'Down',
  Left = 'Left',
}

export function getDirection(line: string): Direction {
  switch (line.charAt(0)) {
    case 'U': return Direction.Up;
    case 'R': return Direction.Right;
    case 'D': return Direction.Down;
    case 'L':
    default: return Direction.Left;
  }
}

export function getSteps(line: string): number {
  const steps = line.match(/\d+/)?.[0];
  return Number(steps || '0');
}

export type Location = {
  x: number,
  y: number,
};

export function getNextHead(head: Location, direction: Direction): Location {
  switch (direction) {
    case Direction.Up: return { ...head, y: head.y - 1 };
    case Direction.Right: return { ...head, x: head.x + 1 };
    case Direction.Down: return { ...head, y: head.y + 1 };
    case Direction.Left: return { ...head, x: head.x - 1 };
    default: return head;
  }
}

export function getNextTail(head: Location, tail: Location): Location {
  const xDiff = head.x - tail.x;
  const yDiff = head.y - tail.y;
  const totalDiff = Math.abs(xDiff) + Math.abs(yDiff);
  if (Math.abs(xDiff) <= 1 && Math.abs(yDiff) <= 1) {
    return tail;
  }

  let tailXMovement = 0;
  if (totalDiff === 3) {
    tailXMovement = xDiff;
  }
  if (xDiff !== 0) {
    if (totalDiff === 3 && Math.abs(xDiff) === 1) {
      tailXMovement = xDiff;
    } else {
      tailXMovement = xDiff < 0 ? xDiff + 1 : xDiff - 1;
    }
  }
  let tailYMovement = 0;
  if (yDiff !== 0) {
    if (totalDiff === 3 && Math.abs(yDiff) === 1) {
      tailYMovement = yDiff;
    } else {
      tailYMovement = yDiff < 0 ? yDiff + 1 : yDiff - 1;
    }
  }
  const nextTail = {
    x: tail.x + tailXMovement,
    y: tail.y + tailYMovement,
  };
  return nextTail;
}

export function getTailLocationCt(lines: string[], ropeLength: number): number {
  const knots: Location[] = new Array(ropeLength).fill(undefined).map(() => ({ x: 0, y: 0 }));
  const tailLocations = new Set<string>();
  tailLocations.add(JSON.stringify(knots.at(-1)));
  lines.forEach((line) => {
    const direction = getDirection(line);
    const steps = getSteps(line);
    for (let step = 0; step < steps; step += 1) {
      knots[0] = getNextHead(knots[0], direction);
      for (let i = 0; i < knots.length - 1; i += 1) {
        knots[i + 1] = getNextTail(knots[i], knots[i + 1]);
      }
      tailLocations.add(JSON.stringify(knots.at(-1)));
    }
  });
  return tailLocations.size;
}

export default function day9(input: string) {
  const lines = input.split('\n');
  console.log(
    'Simulate your complete hypothetical series of motions.'
    + 'How many positions does the tail of the rope visit at least once?',
    getTailLocationCt(lines, 2),
  );

  console.log(
    'Simulate your complete series of motions on a larger rope with ten knots.'
    + 'How many positions does the tail of the rope visit at least once?',
    getTailLocationCt(lines, 10),
  );
}
