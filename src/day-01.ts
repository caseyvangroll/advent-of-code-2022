// https://adventofcode.com/2022/day/1

export default function day1(input: string) {
  const elfInventories: string[][] = input.split(/\n\n+/g).map((elfInventory) => elfInventory.split('\n'));

  const summedElfInventories: number[] = elfInventories
    .map((elfInventory) => elfInventory.reduce((acc, n) => acc + Number(n), 0));

  summedElfInventories.sort((a, b) => b - a);

  const mostCaloriesInAnInventory = summedElfInventories[0];
  console.log(
    'Find the Elf carrying the most Calories. How many total Calories is that Elf carrying?',
    mostCaloriesInAnInventory,
  );

  const sumOf3MostCaloricInventories = summedElfInventories
    .slice(0, 3).reduce((acc, n) => acc + n, 0);
  console.log(
    'Find the top three Elves carrying the most Calories. How many Calories are those Elves carrying in total?',
    sumOf3MostCaloricInventories,
  );
}
