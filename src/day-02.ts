// https://adventofcode.com/2022/day/2

export enum HandShape {
  Rock = 'Rock',
  Paper = 'Paper',
  Scissors = 'Scissors',
}

export function mapCharToHandShape(char: string): HandShape {
  switch (char) {
    case 'A':
    case 'X':
      return HandShape.Rock;
    case 'B':
    case 'Y':
      return HandShape.Paper;
    case 'C':
    case 'Z':
      return HandShape.Scissors;
    default:
      throw new Error(`Unrecognized hand shape char: ${char}`);
  }
}

export function getHandShapeScore(handShape: HandShape): number {
  switch (handShape) {
    case HandShape.Rock:
      return 1;
    case HandShape.Paper:
      return 2;
    case HandShape.Scissors:
    default:
      return 3;
  }
}

export function getRoundScorePartOne(line: string) {
  const [opponentPlay, myPlay] = line.split(' ').map(mapCharToHandShape);
  let roundScore = getHandShapeScore(myPlay);

  if (opponentPlay === myPlay) {
    roundScore += 3;
  } else if (
    (opponentPlay === HandShape.Rock && myPlay === HandShape.Paper)
    || (opponentPlay === HandShape.Paper && myPlay === HandShape.Scissors)
    || (opponentPlay === HandShape.Scissors && myPlay === HandShape.Rock)
  ) {
    roundScore += 6;
  }

  return roundScore;
}

export enum Outcome {
  Win = 'Win',
  Loss = 'Loss',
  Tie = 'Tie',
}

export function mapCharToOutcome(char: string): Outcome {
  switch (char) {
    case 'X':
      return Outcome.Loss;
    case 'Y':
      return Outcome.Tie;
    case 'Z':
      return Outcome.Win;
    default:
      throw new Error(`Unrecognized outcome code: ${char}`);
  }
}

export function getLosingPlay(opponentPlay: HandShape) {
  switch (opponentPlay) {
    case HandShape.Rock:
      return HandShape.Scissors;
    case HandShape.Paper:
      return HandShape.Rock;
    case HandShape.Scissors:
    default:
      return HandShape.Paper;
  }
}

export function getWinningPlay(opponentPlay: HandShape) {
  switch (opponentPlay) {
    case HandShape.Rock:
      return HandShape.Paper;
    case HandShape.Paper:
      return HandShape.Scissors;
    case HandShape.Scissors:
    default:
      return HandShape.Rock;
  }
}

export function getRoundScorePartTwo(line: string) {
  const [encodedOpponentPlay, encodedDesiredOutcome] = line.split(' ');
  const opponentPlay = mapCharToHandShape(encodedOpponentPlay);
  const desiredOutcome = mapCharToOutcome(encodedDesiredOutcome);
  if (desiredOutcome === Outcome.Tie) {
    return 3 + getHandShapeScore(opponentPlay);
  }
  if (desiredOutcome === Outcome.Win) {
    return 6 + getHandShapeScore(getWinningPlay(opponentPlay));
  }

  return getHandShapeScore(getLosingPlay(opponentPlay));
}

export default function day2(input: string) {
  let totalScorePartOne = 0;
  let totalScorePartTwo = 0;
  input.replace(/\r/g, '').split('\n').forEach((line) => {
    totalScorePartOne += getRoundScorePartOne(line);
    totalScorePartTwo += getRoundScorePartTwo(line);
  });

  console.log(
    'What would your total score be if everything goes exactly according to your strategy guide? ',
    totalScorePartOne,
  );
  console.log(
    "Following the Elf's instructions for the second column, what would your total score"
    + ' be if everything goes exactly according to your strategy guide?',
    totalScorePartTwo,
  );
}
