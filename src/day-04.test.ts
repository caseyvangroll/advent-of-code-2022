import { doesOneFullyContainTheOther, doesOverlap, getGroups } from './day-04';

describe('doesOneFullyContainTheOther', () => {
  test.each`
    line            | expected
    ${'2-4,6-8'}    | ${false}
    ${'2-3,4-5'}    | ${false}
    ${'5-7,7-9'}    | ${false}
    ${'2-8,3-7'}    | ${true}
    ${'6-6,4-6'}    | ${true}
    ${'2-6,4-8'}    | ${false}
    ${'2-95,94-99'} | ${false}
  `(
    'doesOneFullyContainTheOther($line) => $expected',
    ({
      line, expected,
    }) => {
      const [minA, maxA, minB, maxB] = getGroups(line);
      expect(doesOneFullyContainTheOther(minA, maxA, minB, maxB)).toEqual(expected);
    },
  );
});

describe('doesOverlap', () => {
  test.each`
    line             | expected
    ${'2-4,6-8'}     | ${false}
    ${'2-3,4-5'}     | ${false}
    ${'5-7,7-9'}     | ${true}
    ${'2-8,3-7'}     | ${true}
    ${'6-6,4-6'}     | ${true}
    ${'2-6,4-8'}     | ${true}
    ${'2-95,94-99'}  | ${true}
    ${'1-1,1-1'}     | ${true}
    ${'35-97,34-98'} | ${true}
    ${'28-98,98-99'} | ${true}
    ${'20-58,58-80'} | ${true}
  `(
    'doesOverlap($line) => $expected',
    ({
      line, expected,
    }) => {
      const [minA, maxA, minB, maxB] = getGroups(line);
      expect(doesOverlap(minA, maxA, minB, maxB)).toEqual(expected);
    },
  );
});
