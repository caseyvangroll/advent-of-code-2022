// https://adventofcode.com/2022/day/6

function howManyCharsUntilSequentialUniqueChars(s: string, uniqueCharSegmentLength: number) {
  for (let i = 0; i <= s.length - uniqueCharSegmentLength; i += 1) {
    const charSet = new Set(s.substring(i, i + uniqueCharSegmentLength));
    if (charSet.size === uniqueCharSegmentLength) {
      return i + uniqueCharSegmentLength;
    }
  }
  return s.length;
}

export default function day6(input: string) {
  const charsUntilFourSequentialUniqueChars = howManyCharsUntilSequentialUniqueChars(input, 4);
  console.log(
    'How many characters need to be processed before the first start-of-packet marker is detected?',
    charsUntilFourSequentialUniqueChars,
  );
  const charsUntilFourteenSequentialUniqueChars = howManyCharsUntilSequentialUniqueChars(input, 14);
  console.log(
    'How many characters need to be processed before the first start-of-message marker is detected?',
    charsUntilFourteenSequentialUniqueChars,
  );
}
