// https://adventofcode.com/2022/day/3

// Lowercase item types a through z have priorities 1 through 26.
// Uppercase item types A through Z have priorities 27 through 52.
export function getPriority(char: string): number {
  const asciiCharCode = char.charCodeAt(0);
  if (asciiCharCode > 90) {
    return asciiCharCode - 96;
  }
  return asciiCharCode - 38;
}

export function getCommonChars(x: string, y: string): Set<string> {
  const xCharMap = new Set(x);
  const commonChars = new Set<string>();

  for (let i = 0; i < y.length; i += 1) {
    if (xCharMap.has(y[i])) {
      commonChars.add(y[i]);
    }
  }

  return commonChars;
}

export function getCommonCompartmentItem(rucksack: string): string {
  const midIndex = rucksack.length / 2;
  const lCompartment = rucksack.slice(0, midIndex);
  const rCompartment = rucksack.slice(midIndex);
  return getCommonChars(lCompartment, rCompartment).values().next().value;
}

export default function day3(input: string) {
  const rucksacks = input.split('\n');
  let sumOfCommonItemPriorities = 0;
  rucksacks.forEach((rucksack) => {
    const commonItem = getCommonCompartmentItem(rucksack);
    sumOfCommonItemPriorities += getPriority(commonItem);
  });
  console.log(
    'Find the item type that appears in both compartments of each rucksack.'
    + 'What is the sum of the priorities of those item types?',
    sumOfCommonItemPriorities,
  );

  let sumOfBadgePriorities = 0;
  for (let i = 0; i < rucksacks.length; i += 3) {
    const groupRucksacks = rucksacks.slice(i, i + 3);
    const commonA = getCommonChars(groupRucksacks[0], groupRucksacks[1]);
    const commonB = getCommonChars(groupRucksacks[1], groupRucksacks[2]);
    const badge = [...commonA].filter((char) => commonB.has(char))[0];
    sumOfBadgePriorities += getPriority(badge);
  }

  console.log(
    'Find the item type that corresponds to the badges of each three-Elf group.'
    + 'What is the sum of the priorities of those item types?',
    sumOfBadgePriorities,
  );
}
