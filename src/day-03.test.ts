import { getPriority } from './day-03';

describe('getPriority', () => {
  test('a = 1', () => {
    expect(getPriority('a')).toEqual(1);
  });

  test('z = 26', () => {
    expect(getPriority('z')).toEqual(26);
  });

  test('A = 27', () => {
    expect(getPriority('A')).toEqual(27);
  });

  test('Z = 52', () => {
    expect(getPriority('Z')).toEqual(52);
  });
});
